import { Inject, Injectable, Type, Optional } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import * as GoldenLayout from 'golden-layout';
import { GoldenLayoutConfiguration, ComponentConfiguration, GoldenLayoutDefaultSettings } from './config';
import { GoldenLayoutStateStore, StateStore, LocalStorageStateStore } from './state';
import * as _ from 'lodash'
/**
 * golden-layout component initialization callback type.
 */
export interface ComponentInitCallback extends Function {
  (container: GoldenLayout.Container, componentState: any): void;
}

export interface ComponentInitCallbackFactory { 
  createComponentInitCallback(component: Type<any>): ComponentInitCallback;
}

@Injectable()
export class GoldenLayoutService {
  public layoutSettings:any;
  private _goldenLayout:GoldenLayout;
  constructor(@Inject(GoldenLayoutConfiguration) public readonly config: GoldenLayoutConfiguration,
              @Optional() @Inject(GoldenLayoutStateStore) private readonly stateStore: StateStore) {
                this.layoutSettings = GoldenLayoutDefaultSettings;
                if(_.has(this.config,'layoutSettings')){
                  this.layoutSettings = _.assign({}, this.layoutSettings,this.config.layoutSettings);
                }
                console.log("layoutSettings",this.layoutSettings);
              }

  public initialize(goldenLayout: GoldenLayout, componentInitCallbackFactory: ComponentInitCallbackFactory) {
    this._goldenLayout = goldenLayout;
    this.config.components.forEach((componentConfig: ComponentConfiguration) => {
      const componentInitCallback = componentInitCallbackFactory.createComponentInitCallback(componentConfig.component);
      goldenLayout.registerComponent(componentConfig.componentName, componentInitCallback);
    });

    if (this.stateStore) {
      (<GoldenLayout.EventEmitter>(<any>goldenLayout)).on('stateChanged', () => {
        this._saveState(goldenLayout);
      });
    }
  }

  private _saveState(goldenLayout: GoldenLayout): void {
    if (this.stateStore && goldenLayout.isInitialised && this.layoutSettings.persistLayoutState) {
      try {
        this.stateStore.writeState(goldenLayout.toConfig());
      } catch(ex) {
        // Workaround for https://github.com/deepstreamIO/golden-layout/issues/192
      }
    }
  }

  public getState(): Promise<any>{
    if (this.stateStore && this.layoutSettings.persistLayoutState) {
      return this.stateStore.loadState().catch(() => {
        return this.config.defaultLayout;
      });
    } else{
      this.stateStore.clearState();
    }

    return Promise.resolve(this.config.defaultLayout);
  }

  public getGoldenLayout():GoldenLayout{
    return this._goldenLayout;
  }

  public isInitialised():boolean {
    return this._goldenLayout.isInitialised;
  }
}
